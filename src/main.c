#include "tests.h"

int main() {
    common_allocate_test();
    free_one_block_test();
    free_two_blocks_test();
    region_extension_test();
    region_continue_in_another_place_test();
    return 0;
}

