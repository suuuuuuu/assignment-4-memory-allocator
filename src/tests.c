#include "string.h"
#include "ctype.h"
#include "tests.h"

void print_tests_block_name(const char* name);
void print_testing_success(const char* reason);
void print_testing_msg(const char* testing_object_name);
void print_testing_error(const char* reason);

struct block_header* to_block_header(void* block) {
    return block - offsetof(struct block_header, contents);
}

void* get_block_after(struct block_header const* block) {
    return (void*) (block->contents + block->capacity.bytes);
}

bool common_allocate_test() {
    print_tests_block_name("Common allocation test");
    print_testing_msg("Common heap allocation");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(heap == NULL) {
        print_testing_error("Heap allocation");
        return false;
    }
    print_testing_success("Heap allocation");
    print_testing_msg("Full single block allocation");
    void *block_alloc = _malloc(REGION_MIN_SIZE);
    if (block_alloc == NULL || to_block_header(block_alloc)->is_free) {
        print_testing_error("Block allocation");
        return false;
    }
    print_testing_success("Block allocation");
    print_testing_msg("Full single block free");
    _free(block_alloc);
    if (to_block_header(block_alloc)->is_free == false) {
        print_testing_error("Free");
        return false;
    }
    print_testing_success("Free");
    munmap(heap, MIN_REGION_MUN_MAP_SIZE);

    return true;
}

bool free_one_block_test() {
    print_tests_block_name("Free second block (4 at all)");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(heap == NULL){
        print_testing_error("Heap allocation");
        return false;
    }
    print_testing_msg("4 blocks allocation");
    void *first_block_alloc = _malloc(BLOCK_MIN_CAPACITY);
    void *second_block_alloc = _malloc(BLOCK_MIN_CAPACITY * 2);
    void *third_block_alloc = _malloc(BLOCK_MIN_CAPACITY * 3);
    void *fourth_block_alloc = _malloc(BLOCK_MIN_CAPACITY * 5);
    if (first_block_alloc == NULL || second_block_alloc == NULL ||
            third_block_alloc == NULL || fourth_block_alloc == NULL) {
        print_testing_error("4 blocks allocation");
        return false;
    }
    print_testing_success("4 blocks allocation");
    print_testing_msg("Free second block and placing new there");
    _free(second_block_alloc);
    void* trying_alloc_after_free = _malloc(BLOCK_MIN_CAPACITY);
    if (trying_alloc_after_free == NULL || to_block_header(trying_alloc_after_free)->is_free) {
        print_testing_error("Free second block");
        return false;
    }
    if (to_block_header(trying_alloc_after_free)->contents != to_block_header(second_block_alloc)->contents) {
        print_testing_error("Placing new there");
        return false;
    }
    print_testing_success("Free second block and placing new there");
    munmap(heap, MIN_REGION_MUN_MAP_SIZE); // hard to calc outside mem.c without its internal functions

    return true;
}

bool free_two_blocks_test() {
    print_tests_block_name("Free second and last blocks (5 at all)");
    void* heap = heap_init(REGION_MIN_SIZE);
    if(heap == NULL){
        print_testing_error("Heap allocation");
        return false;
    }
    void *first_block_alloc = _malloc(BLOCK_MIN_CAPACITY);
    void *second_block_alloc = _malloc(BLOCK_MIN_CAPACITY * 2);
    void *third_block_alloc = _malloc(BLOCK_MIN_CAPACITY);
    void *fourth_block_alloc = _malloc(BLOCK_MIN_CAPACITY * 5);
    void *fifth_block_alloc = _malloc(BLOCK_MIN_CAPACITY * 10);
    if (first_block_alloc == NULL || second_block_alloc == NULL ||
        third_block_alloc == NULL || fourth_block_alloc == NULL ||
        fifth_block_alloc == NULL) {
        print_testing_error("5 blocks allocation");
        return false;
    }
    print_testing_msg("Free second and last blocks and placing new ones there");
    _free(second_block_alloc);
    _free(fifth_block_alloc);
    void* trying_alloc_on_second_place = _malloc(BLOCK_MIN_CAPACITY * 2);
    void* trying_alloc_on_last_place = _malloc(BLOCK_MIN_CAPACITY * 10);
    if (trying_alloc_on_second_place == NULL || trying_alloc_on_last_place == NULL) {
        print_testing_error("Free second or last block");
        return false;
    }
    if (to_block_header(trying_alloc_on_second_place)->contents != to_block_header(second_block_alloc)->contents ||
            to_block_header(trying_alloc_on_last_place)->contents != to_block_header(fifth_block_alloc)->contents) {
        print_testing_error("Placing new on second or last block place");
        return false;
    }
    print_testing_success("Free second and last blocks and placing new ones there");
    munmap(heap, MIN_REGION_MUN_MAP_SIZE); // hard to calc outside mem.c without its internal functions

    return true;
}

bool region_extension_test() {
    print_tests_block_name("Region extension");
    void *heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL) {
        print_testing_error("Heap allocation");
        return false;
    }
    print_testing_msg("Region overflow - doing region extension");
    _malloc(REGION_MIN_SIZE / 2); // first block alloc
    void *second_block_alloc = _malloc(REGION_MIN_SIZE / 2);
    void *third_block_alloc = _malloc(REGION_MIN_SIZE / 2);
    if (third_block_alloc == NULL ||
            get_block_after(to_block_header(second_block_alloc)) != to_block_header(third_block_alloc)) {
        print_testing_error("Region extension");
        return false;
    }
    print_testing_success("Region overflow - doing region extension");
    munmap(heap, MIN_REGION_MUN_MAP_SIZE + REGION_MIN_SIZE);

    return true;
}

#define MAP_ANONYMOUS_TEST_FIX 0x20
#define MAP_FIXED_NOREPLACE_TEST_FIX 0x100000

bool region_continue_in_another_place_test() {
    print_tests_block_name("Region continue in another place");
    void *heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL) {
        print_testing_error("Heap allocation");
        return false;
    }
    void *first_region_block = _malloc(123);
    if (first_region_block == NULL) {
        print_testing_error("First region block malloc");
        return false;
    }
    void *in_between = mmap(HEAP_START + MIN_REGION_MUN_MAP_SIZE,
                            REGION_MIN_SIZE,
                            PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS_TEST_FIX | MAP_FIXED_NOREPLACE_TEST_FIX, -1, 0);
    if (in_between == MAP_FAILED) {
        print_testing_error("mmap");
        return false;
    }
    print_testing_msg("Region continue in another place");
    char * second_region_block = _malloc(MIN_REGION_MUN_MAP_SIZE);
    if (second_region_block == NULL) {
        print_testing_error("Second region block malloc");
        return false;
    }
    if (get_block_after(to_block_header(first_region_block)) == to_block_header(second_region_block)) {
        print_testing_error("Second region block is continue of first");
        return false;
    }
    print_testing_success("Region continue in another place");

    return true;
}

void print_tests_block_name(const char* name) {
    printf("\n---");
    for (int a = 0; a < strlen(name); a++) {
        printf("%c", toupper(name[a]));
    }
    printf("---\n");
}

void print_testing_success(const char* reason) {
    printf("✓ %s success\n", reason);
}

void print_testing_msg(const char* testing_object_name) {
    printf("Testing %c%s\n", tolower(testing_object_name[0]), testing_object_name + 1);
}

void print_testing_error(const char* reason) {
    printf("|x| %s error\n", reason);
    fprintf(stderr,"%s error\n", reason);
}