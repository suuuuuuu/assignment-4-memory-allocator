#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"

#define MIN_REGION_MUN_MAP_SIZE 12288 // hard to calc outside mem.c without its internal functions

bool common_allocate_test();
bool free_one_block_test();
bool free_two_blocks_test();
bool region_extension_test();
bool region_continue_in_another_place_test();

#endif //MEMORY_ALLOCATOR_TESTS_H
