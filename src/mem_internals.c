#include "mem_internals.h"

struct region init_region(void* region_start, size_t region_size, bool extends) {
    struct region allocated_region = (struct region) {
            .addr = region_start,
            .extends = extends,
            .size = region_size
    };
    if (region_is_invalid(&allocated_region)) {
        return REGION_INVALID;
    }
    // нужно инициализировать начальный блок - который и будем делить
    block_init(region_start, init_size(region_size), NULL);
    return allocated_region;
}

bool region_is_invalid(const struct region* r) {
    return r->addr == NULL;
}

block_size size_from_capacity(block_capacity cap) {
    return (block_size) {
            cap.bytes + offsetof(struct block_header, contents)
    };
}

block_capacity capacity_from_size(block_size sz) {
    return (block_capacity) {
            sz.bytes - offsetof(struct block_header, contents)
    };
}

block_capacity init_capacity(size_t bytes) {
    return (block_capacity) {
            .bytes = bytes
    };
}

block_size init_size(size_t bytes) {
    return (block_size) {
            .bytes = bytes
    };
}

void block_init(void* restrict addr, block_size block_size, void* restrict next) {
    *((struct block_header*) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_size),
            .is_free = true
    };
}
