#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ...);
void debug(const char* fmt, ...);

static bool block_is_big_enough(block_capacity query_capacity, struct block_header* block);
static size_t pages_count(block_size block_size);
static size_t round_pages_size(block_size block_size);

static size_t region_actual_size(size_t block_size_bytes);

static void* map_pages(void const* addr, size_t length, int additional_flags);

static bool block_is_big_enough(block_capacity query_capacity, struct block_header* block) {
    return block->capacity.bytes >= query_capacity.bytes;
}

static size_t pages_count(block_size block_size) {
    return block_size.bytes / getpagesize() + ((block_size.bytes % getpagesize() > 0) ? 1 : 0);
}

static size_t round_pages_size(block_size block_size) {
    return getpagesize() * pages_count(block_size);
}


static size_t region_actual_size(size_t block_size_bytes) {
    return size_max(round_pages_size(init_size(block_size_bytes)), REGION_MIN_SIZE);
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap((void*) addr,
                length,
                PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | additional_flags ,
                -1,
                0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const * addr, size_t capacity_bytes) {
    block_capacity capacity = init_capacity(capacity_bytes);
    block_size block_size = size_from_capacity(capacity);
    size_t region_size = region_actual_size(block_size.bytes);
    void* region_start = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
    if (region_start == MAP_FAILED) {
        // место аллоцирования теперь выбирает система
        region_start = map_pages(addr, region_size, 0);
        if (region_start == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    return init_region(region_start, region_size, region_start == addr);
}

static void* block_after(struct block_header const* block);

void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, block_capacity query_capacity) {
    block_size query_size = size_from_capacity(query_capacity);

    return block->is_free &&
        query_size.bytes + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//если можно рассплитить, инициализируем лишний блок, а старый ограничиваем только необходимой памятью.
static bool split_if_too_big( struct block_header* block, size_t query ) {
    if ( !block_splittable(block, init_capacity(query))) return false;
    //указатель на конец необходимой памяти
    void* ad = (*block).contents + query;
    //инициализируем блок памяти после нужной нам, размер - всего байтов в исходном splittable блоке - то
    //количество байт, что на нужно, указатель на следующий блок передаём из исиходного.
    block_init( ad, (block_size) { (*block).capacity.bytes - query }, (*block).next);
    //устанавливаем размер нужного нам блока на необходимый
    (*block).capacity.bytes = query;
    //ставим в ссылку на следующий блок наш "фришный"
    (*block).next = ad;

    return true;
}




/*  --- Слияние соседних свободных блоков --- */

static void* get_block_after(struct block_header const* block) {
    return (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(struct block_header const* block,
                               struct block_header const* block_after) {
    return get_block_after(block) == (void*) block_after;
}

static bool mergeable(struct block_header const* restrict block, struct block_header const* restrict block_after) {
    return block->is_free && block_after->is_free &&
        blocks_continuous(block, block_after) ;
}

static bool try_merge_with_next(struct block_header* block) {
    if (block->next == NULL || !mergeable(block, block->next)) {
        return false;
    }
    block_size block_after_size = size_from_capacity(block->next->capacity);
    block->capacity.bytes += block_after_size.bytes;
    block->next = block->next->next;

    return true;
}


/*  --- ... ecли размера кучи хватает --- */


struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED
    } type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t query_capacity_bytes)    {
    if (block == NULL) return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
    struct block_header* last = block;
    do {
        if (!block->is_free) {
            block = block->next;
            continue;
        }
        while(try_merge_with_next(block));
        if (block_is_big_enough(init_capacity(query_capacity_bytes), block)) {
            return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
        }
        last = block;
        block = block->next;
    } while (block != NULL);

    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = last};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
    Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query_capacity_bytes, struct block_header* block) {
    struct block_search_result find_res = find_good_or_last(block, query_capacity_bytes);
    if (find_res.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(find_res.block, query_capacity_bytes);
        find_res.block->is_free = false;
    }

    return find_res;
}




static struct block_header* grow_heap(struct block_header* restrict last, size_t capacity_bytes) {
    struct region allocated_region = alloc_region(get_block_after(last), capacity_bytes);
    if (region_is_invalid(&allocated_region)) {
        return NULL;
    }
    last->next = allocated_region.addr;

    return try_merge_with_next(last) ? last : allocated_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query_capacity_size, struct block_header* heap_start) {
    size_t memalloc_capacity_size = size_max(query_capacity_size, BLOCK_MIN_CAPACITY);
    struct block_search_result memalloc_res = try_memalloc_existing(memalloc_capacity_size, heap_start);
    struct block_header *memalloc_block_after_grow; // for BSR_REACHED_END_NOT_FOUND case
    switch (memalloc_res.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return memalloc_res.block;
        case BSR_REACHED_END_NOT_FOUND:
            memalloc_block_after_grow = grow_heap(memalloc_res.block, memalloc_capacity_size);
            return try_memalloc_existing(memalloc_capacity_size, memalloc_block_after_grow).block;
        case BSR_CORRUPTED:
            return NULL;
    }

    return NULL; // "C" moment xd
}

void* _malloc(size_t query) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
  if (!mem) return;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}